# Volatility 2

Volatility 2 is an application for analysing forensic memory images, which is written in Python 2.

I've run into issues when trying to run Python 2 on my desktop, so have decided to containerise it. This stops my local environment from getting messy with loads of different Python packages installed, different virtual environments and makes it easier to run.


## Building the Image

From within this directory, run:

```bash
docker build . -t vol2
```

## Using the Image

From within the directory which has the memory sample you wish to analyse, run:

```bash
docker run -it --rm -v "$(pwd)":/images vol2:latest bash
```

Once you have a terminal prompt inside of the container, you can analyse memory images by running:

```bash
python /vol2/vol.py -f /images/memory.img <command>
```
